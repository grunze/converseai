package com.converseai.jsonStructure.receiveDomain;

import java.util.List;

public class PayloadAttachment {
	
	private List<Elements> elements;
	private String template_type;
	private String text;
	private List<Buttons> buttons;
	
	public PayloadAttachment() {
	}
	
	public List<Elements> getElements() {
		return elements;
	}
	public void setElements(List<Elements> elements) {
		this.elements = elements;
	}
	public String getTemplate_type() {
		return template_type;
	}
	public void setTemplate_type(String template_type) {
		this.template_type = template_type;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public List<Buttons> getButtons() {
		return buttons;
	}

	public void setButtons(List<Buttons> buttons) {
		this.buttons = buttons;
	}

	@Override
	public String toString() {
		return "PayloadAttachment [elements=" + elements + ", template_type=" + template_type + ", text=" + text
				+ ", buttons=" + buttons + "]";
	}
	
	
	
}
