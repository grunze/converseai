package com.converseai.jsonStructure.receiveDomain;

public class Payload {
	
	private Message message;
	private Threads thread;
	
	public Payload() {
	}

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	public Threads getThread() {
		return thread;
	}

	public void setThread(Threads thread) {
		this.thread = thread;
	}

	@Override
	public String toString() {
		return "Payload [message=" + message + ", thread=" + thread + "]";
	}
	
	
	
}
