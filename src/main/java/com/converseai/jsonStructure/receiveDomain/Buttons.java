package com.converseai.jsonStructure.receiveDomain;

public class Buttons {

	private String payload;
	private String title;
	private String type;
	
	public Buttons() {
	}

	public String getPayload() {
		return payload;
	}

	public void setPayload(String payload) {
		this.payload = payload;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Buttons [payload=" + payload + ", title=" + title + ", type=" + type + "]";
	}
	
	
}
