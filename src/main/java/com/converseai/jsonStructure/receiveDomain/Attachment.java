package com.converseai.jsonStructure.receiveDomain;

public class Attachment {
	
	private PayloadAttachment payload;

	public Attachment() {
	}
	
	public PayloadAttachment getPayload() {
		return payload;
	}

	public void setPayload(PayloadAttachment payload) {
		this.payload = payload;
	}

	@Override
	public String toString() {
		return "Attachment [payload=" + payload + "]";
	}
	
	
	
}
