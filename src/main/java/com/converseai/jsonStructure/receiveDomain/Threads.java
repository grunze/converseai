package com.converseai.jsonStructure.receiveDomain;

public class Threads {

	private String userID;
	private String threadID;
	private String firstName;
	private String lastName;
	private String email;

	public Threads() {
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getThreadID() {
		return threadID;
	}

	public void setThreadID(String threadID) {
		this.threadID = threadID;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "Threads [userID=" + userID + ", threadID=" + threadID + ", firstName=" + firstName + ", lastName="
				+ lastName + ", email=" + email + "]";
	}

	
}
