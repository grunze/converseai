package com.converseai.jsonStructure.receiveDomain;

import java.util.List;

public class Elements {

	private List <Buttons> buttons;
	private String image_url;
	private String subtitle;
	private String title;
	
	public Elements() {
	}

	public List<Buttons> getButtons() {
		return buttons;
	}

	public void setButtons(List<Buttons> buttons) {
		this.buttons = buttons;
	}

	public String getImage_url() {
		return image_url;
	}

	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String toString() {
		return "Elements [buttons=" + buttons + ", image_url=" + image_url + ", subtitle=" + subtitle + ", title="
				+ title + "]";
	}
	
	
	
	
}
