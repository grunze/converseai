package com.converseai.jsonStructure.receiveDomain;

public class Body {
	
	private String action;
	private String verifyString;
	private Payload payload;
	
	public Body() {
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getVerifyString() {
		return verifyString;
	}

	public void setVerifyString(String verifyString) {
		this.verifyString = verifyString;
	}

	public Payload getPayload() {
		return payload;
	}

	public void setPayload(Payload payload) {
		this.payload = payload;
	}

	@Override
	public String toString() {
		return "Body [action=" + action + ", verifyString=" + verifyString + ", payload=" + payload + "]";
	}

	
	
}
