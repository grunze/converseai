package com.converseai.jsonStructure.receiveDomain;

public class MediaData {

	private Element element;
	private String type;

	public MediaData() {
	}

	public Element getElement() {
		return element;
	}

	public void setElement(Element element) {
		this.element = element;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "MediaData [element=" + element + ", type=" + type + "]";
	}
}
