package com.converseai.jsonStructure.receiveDomain;

public class Message {

	private String threadID;
	private String userID;
	private Boolean isRichMedia;
	private Double time;
	private String conversationUUID;
	private String messageID;
	private String text;
	private MediaData mediaData;
	private String runtimeCTX;

	public Message() {
	}

	public String getThreadID() {
		return threadID;
	}

	public void setThreadID(String threadID) {
		this.threadID = threadID;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public Boolean getIsRichMedia() {
		return isRichMedia;
	}

	public void setIsRichMedia(Boolean isRichMedia) {
		this.isRichMedia = isRichMedia;
	}

	public Double getTime() {
		return time;
	}

	public void setTime(Double time) {
		this.time = time;
	}

	public String getConversationUUID() {
		return conversationUUID;
	}

	public void setConversationUUID(String conversationUUID) {
		this.conversationUUID = conversationUUID;
	}

	public String getMessageID() {
		return messageID;
	}

	public void setMessageID(String messageID) {
		this.messageID = messageID;
	}

	public MediaData getMediaData() {
		return mediaData;
	}

	public void setMediaData(MediaData mediaData) {
		this.mediaData = mediaData;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getRuntimeCTX() {
		return runtimeCTX;
	}

	public void setRuntimeCTX(String runtimeCTX) {
		this.runtimeCTX = runtimeCTX;
	}
	
	@Override
	public String toString() {
		return "Message [threadID=" + threadID + ", userID=" + userID + ", isRichMedia=" + isRichMedia + ", time="
				+ time + ", conversationUUID=" + conversationUUID + ", messageID=" + messageID + ", text=" + text
				+ ", mediaData=" + mediaData + ", runtimeCTX=" + runtimeCTX + "]";
	}	

}
