package com.converseai.jsonStructure.receiveDomain;

import java.util.List;

public class Element {

	private List<QuickReplies> quick_replies;
	private Attachment attachment;
	private String text;
	
	public Element() {
	}

	public List<QuickReplies> getQuick_replies() {
		return quick_replies;
	}

	public void setQuick_replies(List<QuickReplies> quick_replies) {
		this.quick_replies = quick_replies;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Attachment getAttachment() {
		return attachment;
	}

	public void setAttachment(Attachment attachment) {
		this.attachment = attachment;
	}

	@Override
	public String toString() {
		return "Element [quick_replies=" + quick_replies + ", attachment=" + attachment + ", text=" + text + "]";
	}
	
}
