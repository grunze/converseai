package com.converseai.jsonStructure.sendWebhook;

public class Challenge {

	private String challenge;

	public Challenge() {
	}

	public String getChallenge() {
		return challenge;
	}

	public void setChallange(String challenge) {
		this.challenge = challenge;
	}

	@Override
	public String toString() {
		return "Challange [challange=" + challenge + "]";
	}
}
