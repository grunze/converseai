package com.converseai.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;


import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.converseai.jsonStructure.receiveDomain.Body;
import com.converseai.jsonStructure.sendWebhook.Challenge;
import com.converseai.temp.CallConverse;
import com.google.gson.Gson;

@RestController
public class ConverseController {

	public CallConverse call = new CallConverse();

	@RequestMapping("/hello")
	public String test() {
		return "hello wats up";
	}

	
	@RequestMapping(value = "/webhook", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<?> update(@RequestBody Body body) {

		String act = body.getAction();

		if (act.equals("challenge")) {
			
			// to create the inbound webhook for converse
			
			Challenge challenge = new Challenge();
			challenge.setChallange(body.getPayload().getMessage().getText());

			return new ResponseEntity<Challenge>(challenge, HttpStatus.OK);

		} else if (act.equals("message")) {
			System.out.println(body.getPayload().getMessage().getText());
			return null;

		}
		return null;
	}

	@RequestMapping(value = "/send", method = RequestMethod.POST)
	public void send(@RequestBody Body body) {
		try {
				
			// inbound webhook given by converse
			
			String postUrl = "https://fusemachines22979.converse.ai/channel/generic_channel/inbound?wp=1EB65EC9-16A7-4BD6-A5C9-4C4F2E3031F9&wph=cb2241c67076fca9e43861e9efc8bb1b034e030a6a5956576f3fad1b9a0981f6";

			Gson gson = new Gson();

			HttpClient httpClient = HttpClientBuilder.create().build();
			HttpPost post = new HttpPost(postUrl);

			StringEntity postingString;

			postingString = new StringEntity(gson.toJson(body));

			post.setEntity(postingString);
			post.setHeader("Content-type", "application/json");

			System.out.println(body.toString());

			httpClient.execute(post);

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	
	
}
