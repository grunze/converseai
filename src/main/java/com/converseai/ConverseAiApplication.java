package com.converseai;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConverseAiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConverseAiApplication.class, args);
	}
}
