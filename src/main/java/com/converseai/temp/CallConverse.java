package com.converseai.temp;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import com.converseai.jsonStructure.receiveDomain.Body;
import com.converseai.jsonStructure.receiveDomain.Message;
import com.converseai.jsonStructure.receiveDomain.Payload;
import com.google.gson.Gson;

public class CallConverse {

	
	private String senderID;
	
	public CallConverse() {
	}
	
	
	public void ConverseCall(String text) {
		try {
			Body body = new Body();
			
			body.setAction("message");
			
			body.setVerifyString("1234");
			
			Payload payload = new Payload();
			Message message = new Message();
			
			// need to be set as required
			message.setUserID("username");
			
			// need to be set as required
			message.setThreadID("threadid");
			
			//send message to be sent to the converse
			message.setText(text);
			
			payload.setMessage(message);
			
			body.setPayload(payload);
			
			//inbound webhook given by converse
			
			String postUrl = "https://fusemachines22979.converse.ai/channel/generic_channel/inbound?wp=1EB65EC9-16A7-4BD6-A5C9-4C4F2E3031F9&wph=cb2241c67076fca9e43861e9efc8bb1b034e030a6a5956576f3fad1b9a0981f6";

			Gson gson = new Gson();

			HttpClient httpClient = HttpClientBuilder.create().build();
			HttpPost post = new HttpPost(postUrl);

			StringEntity postingString;

			postingString = new StringEntity(gson.toJson(body));

			post.setEntity(postingString);
			post.setHeader("Content-type", "application/json");

			System.out.println(body.toString());

			httpClient.execute(post);

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	public String getSenderID() {
		return senderID;
	}


	public void setSenderID(String senderID) {
		this.senderID = senderID;
	}

}
