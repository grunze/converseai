# Connect to the converse.ai

1. Get inbound webhook
	1. First select the **'Custom Channel'** form the **channel** situated at the right side after login.
	
		![](imgs/1.png) 
	
		![](imgs/2.png) 
	
	2. Create the outbound webhook which returns the **text** send by the converse.
			
			Request sent by the converse to the created webhook
			
			{
 				 "action": "challenge",
 				 "verifyString": "anystring",
  				 "payload": {
    					"message": {
      						"text": "8B58A4A4-DEC3-401D-AE3D-A7F19AC97560",
     						"time": 1480034360028
   					 }
  				}
			}
			
			and the webhook should respond with http 200 ok and challenge with value of text i.e. {{payload.message.text.value}} from received for the converse. In below format.
	
			{
 				 "challenge": "8B58A4A4-DEC3-401D-AE3D-A7F19AC97560"
			}
			
		which gives the inbound webhook in which we have to send our query.
		
		Code :
		
			@RequestMapping(value = "/webhook", method = RequestMethod.POST)
			@ResponseBody
			public ResponseEntity<?> update(@RequestBody Body body) {

				String act = body.getAction();

				if (act.equals("challenge")) {
	
					// to create the inbound webhook for converse
	
					Challenge challenge = new Challenge();
					challenge.setChallange(body.getPayload().getMessage().getText());

					return new ResponseEntity<Challenge>(challenge, HttpStatus.OK);

				} else if (act.equals("message")) {
					System.out.println(body.getPayload().getMessage().getText());
					return null;

				}
				return null;
			}
		
		![](imgs/20180115-152107.png)
		
	3. Message are also send in the json format with "action':"message" as shown in below json format.
	
			{
			  "action": "message",
			  "verifyString": "yourverifystring",
			  "payload": {
			    "message": {
			      "userID": "username",
			      "threadID": "threadid",
			      "text": "subscribe"
			    }
			  }
			}
			
		message to be send should be written in {{payload.message.text.value}} 
		It receives the whole json and send to the inbound webhook of converse
		
			@RequestMapping(value = "/send", method = RequestMethod.POST)
			public void send(@RequestBody Body body) {
				try {
					// inbound webhook given by converse
					
					String postUrl = "https://fusemachines22979.converse.ai/channel/generic_channel/inbound?wp=1EB65EC9-16A7-4BD6-A5C9-4C4F2E3031F9&wph=cb2241c67076fca9e43861e9efc8bb1b034e030a6a5956576f3fad1b9a0981f6";

					Gson gson = new Gson();

					HttpClient httpClient = HttpClientBuilder.create().build();
					HttpPost post = new HttpPost(postUrl);

					StringEntity postingString;

					postingString = new StringEntity(gson.toJson(body));

					post.setEntity(postingString);
					post.setHeader("Content-type", "application/json");

					System.out.println(body.toString());

					httpClient.execute(post);

				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

			}
			
			
		To send specific text only then view
		
			com.converseai.temp.CallConverse package.
			
			com.converseai.temp.CallConverse nd = new CallConverse();
			nd.ConverseCall("text to send");
			
			
### Json of messages are present in data and datas folder
### Json object class is created in
		
		com.converseai.jsonStructure.receiveDomain
		com.converseai.jsonStructure.sendWebhook

		
		
		
	
		
	
	